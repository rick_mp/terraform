# Configure the Microsoft Azure Provider
provider "azurerm" {
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
}

#Common resources
resource "azurerm_resource_group" "Terraform" {
  name     = "Terraform"
  location = "South Central US"
}

resource "azurerm_availability_set" "Terraform" {
  name                = "dfsavset"
  location            = "South Central US"
  resource_group_name = "${azurerm_resource_group.Terraform.name}"

  tags {
    environment = "Production"
  }
}

resource "azurerm_virtual_network" "test" {
  name                = "acctvn"
  address_space       = ["10.0.0.0/16"]
  location            = "South Central US"
  resource_group_name = "${azurerm_resource_group.Terraform.name}"
}

resource "azurerm_subnet" "test" {
  name                 = "acctsub"
  resource_group_name  = "${azurerm_resource_group.Terraform.name}"
  virtual_network_name = "${azurerm_virtual_network.test.name}"
  address_prefix       = "10.0.2.0/24"
}

#end common resources

#Network
resource "azurerm_public_ip" "test" {
  name                         = "WebAppVM-ip"
  location                     = "South Central US"
  resource_group_name          = "${azurerm_resource_group.Terraform.name}"
  public_ip_address_allocation = "dynamic"

  tags {
    environment = "staging"
  }
}

resource "azurerm_network_security_group" "test" {
  name                = "WebAppVM-nsg"
  location            = "South Central US"
  resource_group_name = "${azurerm_resource_group.Terraform.name}"

  security_rule {
    name                       = "allow-rdp"
    priority                   = 1000
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "IIS"
    priority                   = 1010
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags {
    environment = "staging"
  }
}

resource "azurerm_network_interface" "test" {
  name                = "acctni"
  location            = "South Central US"
  resource_group_name = "${azurerm_resource_group.Terraform.name}"

  ip_configuration {
    name                          = "testconfiguration1"
    primary                       = "true"
    subnet_id                     = "${azurerm_subnet.test.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.test.id}"
  }

#Attach another ip address to the VM
  #   ip_configuration {
  #   name                          = "testconfiguration2"
  #   subnet_id                     = "${azurerm_subnet.test.id}"
  #   private_ip_address_allocation = "dynamic"
      
  # }
}

#end Network

#Storage
resource "azurerm_storage_account" "test" {
  name                = "accsa2"
  resource_group_name = "${azurerm_resource_group.Terraform.name}"
  location            = "South Central US"
  account_tier        = "Standard"
  account_replication_type = "LRS"

  tags {
    environment = "staging"
  }
}

resource "azurerm_storage_container" "test" {
  name                  = "vhds"
  resource_group_name   = "${azurerm_resource_group.Terraform.name}"
  storage_account_name  = "${azurerm_storage_account.test.name}"
  container_access_type = "private"
}

#end Storage

resource "azurerm_virtual_machine" "WindowsVM" {
  name                  = "WebAppVM"
  location              = "South Central US"
  availability_set_id   = "${azurerm_availability_set.Terraform.id}"
  resource_group_name   = "${azurerm_resource_group.Terraform.name}"
  network_interface_ids = ["${azurerm_network_interface.test.id}"]
  vm_size               = "Standard_DS1_v2"

  storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }

  storage_os_disk {
    name          = "WebAppVM"
    vhd_uri       = "${azurerm_storage_account.test.primary_blob_endpoint}${azurerm_storage_container.test.name}/WebAppVM.vhd"
    caching       = "ReadWrite"
    create_option = "FromImage"
  }

  # Optional data disks. Will be used to create file share
  storage_data_disk {
    name          = "datadisk0"
    vhd_uri       = "${azurerm_storage_account.test.primary_blob_endpoint}${azurerm_storage_container.test.name}/datadisk0.vhd"
    disk_size_gb  = "4095"
    create_option = "Empty"
    lun           = 0
  }

  os_profile {
    computer_name  = "WebAppVM"
    admin_username = "rick.magana"
    admin_password = "M0r3l1a267901"
  }

  os_profile_windows_config {
    provision_vm_agent        = "true"
    enable_automatic_upgrades = "true"
  }

  tags {
    environment = "staging"
  }
}

#####Add Second VM##############################
# resource "azurerm_public_ip" "test2" {
#   name                         = "WebAppVM2-ip"
#   location                     = "South Central US"
#   resource_group_name          = "${azurerm_resource_group.Terraform.name}"
#   public_ip_address_allocation = "dynamic"

#   tags {
#     environment = "staging"
#   }
# }

# resource "azurerm_network_interface" "test2" {
#   name                = "acctni2"
#   location            = "South Central US"
#   resource_group_name = "${azurerm_resource_group.Terraform.name}"

#   ip_configuration {
#     name                          = "testconfiguration1"
#     subnet_id                     = "${azurerm_subnet.test.id}"
#     private_ip_address_allocation = "dynamic"
#     public_ip_address_id          = "${azurerm_public_ip.test2.id}"
#   }
# }

# resource "azurerm_virtual_machine" "WindowsVM2" {
#   name                  = "WebAppVM2"
#   location              = "South Central US"
#   availability_set_id   = "${azurerm_availability_set.Terraform.id}"
#   resource_group_name   = "${azurerm_resource_group.Terraform.name}"
#   network_interface_ids = ["${azurerm_network_interface.test2.id}"]
#   vm_size               = "Standard_DS1_v2"

#   storage_image_reference {
#     publisher = "MicrosoftWindowsServer"
#     offer     = "WindowsServer"
#     sku       = "2016-Datacenter"
#     version   = "latest"
#   }

#   storage_os_disk {
#     name          = "WebAppVM2"
#     vhd_uri       = "${azurerm_storage_account.test.primary_blob_endpoint}${azurerm_storage_container.test.name}/WebAppVM2.vhd"
#     caching       = "ReadWrite"
#     create_option = "FromImage"
#   }

#   # Optional data disks. Will be used to create file share
#   storage_data_disk {
#     name          = "data2disk0"
#     vhd_uri       = "${azurerm_storage_account.test.primary_blob_endpoint}${azurerm_storage_container.test.name}/data2disk0.vhd"
#     disk_size_gb  = "4095"
#     create_option = "Empty"
#     lun           = 0
#   }

#   os_profile {
#     computer_name  = "WebAppVM2"
#     admin_username = "rick.magana"
#     admin_password = "M0r3l1a267901"
#   }

# os_profile_windows_config {
#     provision_vm_agent        = "true"
#     enable_automatic_upgrades = "true"
#   }

#   tags {
#     environment = "staging"
#   }
# }

# #Cluster Redis
# resource "azurerm_redis_cache" "test" {
#   name                = "clustered-test"
#   location            = "${azurerm_resource_group.Terraform.location}"
#   resource_group_name = "${azurerm_resource_group.Terraform.name}"
#   capacity            = 1
#   family              = "P"
#   sku_name            = "Premium"
#   enable_non_ssl_port = false
#   shard_count         = 3
#
#   redis_configuration {
#     maxclients         = "7500"
#     maxmemory_reserved = "2"
#     maxmemory_delta    = "2"
#     maxmemory_policy   = "allkeys-lru"
#   }
# }
#
# output "key" {
#   value = "${azurerm_redis_cache.test.primary_access_key}"
# }


#Provision Linux (Ubuntu VM)


# #Network
# resource "azurerm_public_ip" "UbuntuPublicIp" {
#   name                         = "UbuntuVM-ip"
#   location                     = "South Central US"
#   resource_group_name          = "${azurerm_resource_group.Terraform.name}"
#   public_ip_address_allocation = "dynamic"
#
#   tags {
#     environment = "staging"
#   }
# }
#
# resource "azurerm_network_security_group" "UbuntuSecurityGroup" {
#   name                = "UbuntuVM-nsg"
#   location            = "South Central US"
#   resource_group_name = "${azurerm_resource_group.Terraform.name}"
#
#   security_rule {
#     name                       = "allow-ssh"
#     priority                   = 1000
#     direction                  = "Inbound"
#     access                     = "Allow"
#     protocol                   = "Tcp"
#     source_port_range          = "*"
#     destination_port_range     = "22"
#     source_address_prefix      = "*"
#     destination_address_prefix = "*"
#   }
#
#   tags {
#     environment = "staging"
#   }
# }
#
# resource "azurerm_network_interface" "UbuntuNetworkInterface" {
#   name                = "ubuntuni"
#   location            = "South Central US"
#   resource_group_name = "${azurerm_resource_group.Terraform.name}"
#
#   ip_configuration {
#     name                          = "testconfiguration1"
#     subnet_id                     = "${azurerm_subnet.test.id}"
#     private_ip_address_allocation = "dynamic"
#     public_ip_address_id          = "${azurerm_public_ip.UbuntuPublicIp.id}"
#   }
# }
#
# #end Network
#
# #Storage
# resource "azurerm_storage_account" "UbuntuStorageAccount" {
#   name                = "ubuntusatest"
#   resource_group_name = "${azurerm_resource_group.Terraform.name}"
#   location            = "South Central US"
#   account_type        = "Standard_LRS"
#
#   tags {
#     environment = "staging"
#   }
# }
#
# resource "azurerm_storage_container" "UbuntuStorageContainer" {
#   name                  = "vhds2"
#   resource_group_name   = "${azurerm_resource_group.Terraform.name}"
#   storage_account_name  = "${azurerm_storage_account.UbuntuStorageAccount.name}"
#   container_access_type = "private"
# }
#
# #end Storage
#
# resource "azurerm_virtual_machine" "UbuntuVM" {
#   name                  = "UbuntuVM"
#   location              = "South Central US"
#   resource_group_name   = "${azurerm_resource_group.Terraform.name}"
#   network_interface_ids = ["${azurerm_network_interface.UbuntuNetworkInterface.id}"]
#   vm_size               = "Standard_DS1_v2"
#
#   storage_image_reference {
#     publisher = "Canonical"
#     offer     = "UbuntuServer"
#     sku       = "14.04.2-LTS"
#     version   = "latest"
#   }
#
#   os_profile {
#     computer_name  = "UbuntuMachine"
#     admin_username = "rick.magana"
#     admin_password = "M0r3l1a267901"
#   }
#
#   storage_os_disk {
#     name          = "myosdisk1"
#     vhd_uri       = "${azurerm_storage_account.UbuntuStorageAccount.primary_blob_endpoint}${azurerm_storage_container.UbuntuStorageContainer.name}/myosdisk1.vhd"
#     caching       = "ReadWrite"
#     create_option = "FromImage"
#   }
#
#   tags {
#     environment = "staging"
#   }
# }

