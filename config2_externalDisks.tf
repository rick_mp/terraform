# Configure the Microsoft Azure Provider
provider "azurerm" {
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
}

variable "prefix" {
  description = "The Prefix used for all resources in this example"
   default = "testing"
}


#Common resources
resource "azurerm_resource_group" "Terraform" {
  name     = "Terraform"
  location = "South Central US"
}

resource "azurerm_availability_set" "Terraform" {
  name                = "dfsavset"
  location = "${azurerm_resource_group.Terraform.location}"
  resource_group_name = "${azurerm_resource_group.Terraform.name}"
  managed = true

  tags {
    environment = "Production"
  }
}

resource "azurerm_virtual_network" "test" {
  name                = "acctvn"
  address_space       = ["10.0.0.0/16"]
  location            = "South Central US"
  resource_group_name = "${azurerm_resource_group.Terraform.name}"
}

resource "azurerm_subnet" "test" {
  name                 = "acctsub"
  resource_group_name  = "${azurerm_resource_group.Terraform.name}"
  virtual_network_name = "${azurerm_virtual_network.test.name}"
  address_prefix       = "10.0.2.0/24"
}

#end common resources

#Network
resource "azurerm_public_ip" "test" {
  name                         = "WebAppVM-ip"
  location                     = "South Central US"
  resource_group_name          = "${azurerm_resource_group.Terraform.name}"
  public_ip_address_allocation = "dynamic"

  tags {
    environment = "staging"
  }
}

resource "azurerm_network_security_group" "test" {
  name                = "WebAppVM-nsg"
  location            = "South Central US"
  resource_group_name = "${azurerm_resource_group.Terraform.name}"

  security_rule {
    name                       = "allow-rdp"
    priority                   = 1000
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "IIS"
    priority                   = 1010
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags {
    environment = "staging"
  }
}

resource "azurerm_network_interface" "test" {
  name                = "acctni"
  location            = "South Central US"
  resource_group_name = "${azurerm_resource_group.Terraform.name}"

  ip_configuration {
    name                          = "testconfiguration1"
    subnet_id                     = "${azurerm_subnet.test.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.test.id}"
  }
}

#end Network

#Storage
resource "azurerm_storage_account" "test" {
  name                = "accsa2"
  resource_group_name = "${azurerm_resource_group.Terraform.name}"
  location            = "South Central US"
  account_tier        = "Standard"
  account_replication_type = "LRS"

  tags {
    environment = "staging"
  }
}

resource "azurerm_storage_container" "test" {
  name                  = "vhds"
  resource_group_name   = "${azurerm_resource_group.Terraform.name}"
  storage_account_name  = "${azurerm_storage_account.test.name}"
  container_access_type = "private"
}

#end Storage





resource "azurerm_virtual_machine" "WindowsVM" {
  name                  = "WebAppVM"
  location              = "South Central US"
  availability_set_id   = "${azurerm_availability_set.Terraform.id}"
  resource_group_name   = "${azurerm_resource_group.Terraform.name}"
  network_interface_ids = ["${azurerm_network_interface.test.id}"]
  vm_size               = "Standard_DS1_v2"

 

 storage_os_disk {
        name =  "myosdisk1"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Standard_LRS"
}

 storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }

  os_profile {
    computer_name  = "WebAppVM"
    admin_username = "rick.magana"
    admin_password = "M0r3l1a267901"
  }


  os_profile_windows_config {
    provision_vm_agent        = "true"
    enable_automatic_upgrades = "true"
  }

  tags {
    environment = "staging"
  }
}


resource "azurerm_managed_disk" "external" {
  count                = "${var.number_of_disks}"
  name                 = "${var.prefix}-disk${count.index+1}"
  location             = "${azurerm_resource_group.Terraform.location}"
  resource_group_name  = "${azurerm_resource_group.Terraform.name}"
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = "10"
}

resource "azurerm_virtual_machine_data_disk_attachment" "external" {
  count              = "${var.number_of_disks}"
  managed_disk_id    = "${azurerm_managed_disk.external.*.id[count.index]}"
  virtual_machine_id = "${azurerm_virtual_machine.WindowsVM.id}"
  lun                = "${10+count.index}"
  caching            = "ReadWrite"
}


